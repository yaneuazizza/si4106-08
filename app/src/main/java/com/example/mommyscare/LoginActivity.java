package com.example.android.mommycare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.android.mommycare.Support.SharedPref;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    SharedPref sharedPref;
    EditText email, password;
    CheckBox checkBoxRemember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        sharedPref = new SharedPref(this);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        checkBoxRemember = findViewById(R.id.checkBoxRemember);

    }

    public void login(View view) {
        if (TextUtils.isEmpty(email.getText()) || TextUtils.isEmpty(password.getText())) {
            Dialog dialog = new Dialog(LoginActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView errorText = dialog.findViewById(R.id.textError);
            errorText.setText("Password minimal 8 karakter");
            dialog.show();
        } else {

            mAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            if (checkBoxRemember.isChecked()) {
                                sharedPref.setString(SharedPref.EMAIL, email.getText().toString());
                                sharedPref.setString(SharedPref.PASSWORD, password.getText().toString());
                                sharedPref.setBoolean(SharedPref.formatKey(SharedPref.REMEMBER, mAuth.getCurrentUser().getUid()), true);
                            }

                            if (sharedPref.getBoolean(SharedPref.formatKey(SharedPref.NOT_FIRST_TIME,mAuth.getCurrentUser().getUid()))) {
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            } else {
                                startActivity(new Intent(LoginActivity.this, OnBoarding.class));
                            }
                            finish();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Dialog dialog = new Dialog(LoginActivity.this);
                            dialog.setContentView(R.layout.dialog_field_error);
                            TextView errorText = dialog.findViewById(R.id.textError);
                            errorText.setText(e.getLocalizedMessage());
                            dialog.show();
                        }
                    });
        }
    }

    public void resetPassword(View view) {
        if (TextUtils.isEmpty(email.getText().toString())) {
            Dialog dialog = new Dialog(LoginActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView errorText = dialog.findViewById(R.id.textError);
            errorText.setText("Password minimal 8 karakter");
            dialog.show();
        } else {
            mAuth.sendPasswordResetEmail(email.getText().toString())
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Dialog dialog = new Dialog(LoginActivity.this);
                            dialog.setContentView(R.layout.dialog_field_error);
                            TextView errorText = dialog.findViewById(R.id.textError);
                            errorText.setText("Link Reset Password sudah dikirim ke Email Anda");
                            dialog.show();
                        }
                    })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Dialog dialog = new Dialog(LoginActivity.this);
                    dialog.setContentView(R.layout.dialog_field_error);
                    TextView errorText = dialog.findViewById(R.id.textError);
                    errorText.setText(e.getLocalizedMessage());
                    dialog.show();
                }
            });
        }
    }
}
