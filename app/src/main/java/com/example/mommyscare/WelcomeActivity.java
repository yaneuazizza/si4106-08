package com.example.android.mommycare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.android.mommycare.Support.PopulateDB;
import com.example.android.mommycare.Support.SharedPref;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    public void toLogin(View view) {
        startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
    }

    public void toSignUp(View view) {
        startActivity(new Intent(WelcomeActivity.this, SignupActivity.class));
    }
}
