package com.example.android.mommycare.Pengaturan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.android.mommycare.MainActivity;
import com.example.android.mommycare.R;

public class FeedbackSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_success);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(FeedbackSuccessActivity.this, MainActivity.class);
                i.putExtra("pengaturan",true);
                startActivity(i);
                finish();
            }
        }, 2000);
    }
}
