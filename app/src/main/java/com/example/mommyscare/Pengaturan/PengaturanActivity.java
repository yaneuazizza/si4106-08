package com.example.android.mommycare.Pengaturan;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.android.mommycare.MainActivity;
import com.example.android.mommycare.R;
import com.example.android.mommycare.SplashActivity;
import com.example.android.mommycare.Support.SharedPref;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class PengaturanActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    FirebaseUser mUser;
    DatabaseReference dRef;
    SharedPref sharedPref;

    EditText emailPengaturan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        dRef = FirebaseDatabase.getInstance().getReference();
        sharedPref = new SharedPref(this);

        emailPengaturan = findViewById(R.id.emailPengaturan);
        emailPengaturan.setText(mUser.getEmail());
    }

    public void backArrow(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(PengaturanActivity.this, MainActivity.class);
        i.putExtra("pengaturan",true);
        startActivity(i);
    }

    public void keluarAkun(View view) {
        sharedPref.clearCredential(mUser.getUid());
        mAuth.signOut();
        startActivity(new Intent(PengaturanActivity.this, SplashActivity.class));
        finish();
    }

    public void simpanPerubahan(View view) {
        if (TextUtils.isEmpty(emailPengaturan.getText())) {
            Dialog dialog = new Dialog(PengaturanActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView textError = dialog.findViewById(R.id.textError);
            textError.setText("Kolom tidak boleh kosong");
            dialog.show();
        } else {
            AuthCredential credential = EmailAuthProvider
                    .getCredential(sharedPref.getString(SharedPref.EMAIL), sharedPref.getString(SharedPref.PASSWORD));

            mUser.reauthenticate(credential)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            mUser.updateEmail(emailPengaturan.getText().toString())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            sharedPref.setString(SharedPref.EMAIL, emailPengaturan.getText().toString());
                                            Dialog dialog = new Dialog(PengaturanActivity.this);
                                            dialog.setContentView(R.layout.dialog_tersimpan);
                                            dialog.show();
                                        }
                                    });
                        }
                    });
        }
    }
}
