package com.example.android.mommycare.Pengaturan;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.android.mommycare.MainActivity;
import com.example.android.mommycare.Model.Feedback;
import com.example.android.mommycare.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FeedbackActivity extends AppCompatActivity {

    DatabaseReference dRef;

    EditText namaFeedbcak, pesanFeedback, kontakFeedbcak;
    RadioGroup grupSubjek;
    String subjek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        dRef = FirebaseDatabase.getInstance().getReference();

        namaFeedbcak = findViewById(R.id.namaFeedback);
        pesanFeedback = findViewById(R.id.pesanFeedback);
        kontakFeedbcak = findViewById(R.id.kontakFeedback);

        grupSubjek = findViewById(R.id.grupSubjek);
        grupSubjek.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton selected = findViewById(checkedId);
                subjek = selected.getText().toString();

            }
        });
    }

    public void backArrow(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(FeedbackActivity.this, MainActivity.class);
        i.putExtra("pengaturan",true);
        startActivity(i);
    }

    public void kirimFeedback(View view) {
        if (TextUtils.isEmpty(namaFeedbcak.getText()) || TextUtils.isEmpty(pesanFeedback.getText()) || TextUtils.isEmpty(kontakFeedbcak.getText())) {
            Dialog dialog = new Dialog(FeedbackActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView textError = dialog.findViewById(R.id.textError);
            textError.setText("Kolom tidak boleh kosong");
            dialog.show();
        } else {
            String key = dRef.child("feedback").push().getKey();
            dRef.child("feedback").child(key)
                    .setValue(new Feedback(
                            key,
                            namaFeedbcak.getText().toString(),
                            subjek,
                            pesanFeedback.getText().toString(),
                            kontakFeedbcak.getText().toString()
                    ));

            startActivity(new Intent(FeedbackActivity.this, FeedbackSuccessActivity.class));
            finish();
        }
    }
}
