package com.example.android.mommycare.Pengaturan;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.mommycare.MainActivity;
import com.example.android.mommycare.Model.User;
import com.example.android.mommycare.R;
import com.example.android.mommycare.SignupActivity;
import com.example.android.mommycare.Support.SharedPref;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ProfileActivity extends AppCompatActivity {

    FirebaseUser mUser;
    DatabaseReference dRef;
    StorageReference sRef;
    SharedPref sharedPref;
    EditText namaProfile, telponProfile, passwordProfile;
    ImageView imageProfile;
    String tempFilePath;
    Uri imageUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mUser = FirebaseAuth.getInstance().getCurrentUser();
        dRef = FirebaseDatabase.getInstance().getReference();
        sRef = FirebaseStorage.getInstance().getReference();
        sharedPref = new SharedPref(this);

        namaProfile = findViewById(R.id.namaProfile);
        telponProfile = findViewById(R.id.telponProfile);
        passwordProfile = findViewById(R.id.passwordProfile);
        imageProfile = findViewById(R.id.imageProfile);

        dRef.child("user").child(mUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                Glide.with(ProfileActivity.this)
                        .load(user.getPhotoUrl())
                        .into(imageProfile);
                namaProfile.setText(user.getNama());
                telponProfile.setText(user.getTelpon());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        passwordProfile.setText(sharedPref.getString(SharedPref.PASSWORD));
    }

    public void backArrow(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ProfileActivity.this, MainActivity.class);
        i.putExtra("pengaturan",true);
        startActivity(i);
    }

    public void selectPhoto(View view) {
        String[] dialog_photo = {"Galeri", "Kamera"};
        new AlertDialog.Builder(ProfileActivity.this)
                .setTitle("Pilih Foto Dari")
                .setItems(dialog_photo, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent in;
                        if (which == 0) {
                            in = new Intent(Intent.ACTION_PICK);
                            in.setType("image/*");
                            String[] mimeTypes = {"image/jpeg", "image/png", "image/jpg"};
                            in.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
                            startActivityForResult(in,1);
                        } else {
                            in = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (photoFile != null) {
                                Uri photoUri = FileProvider.getUriForFile(ProfileActivity.this,"com.example.android.mommycare.provider",photoFile);
                                in.putExtra(MediaStore.EXTRA_OUTPUT,photoUri);
                                startActivityForResult(in, 2);
                            }
                        }
                    }
                })
                .show();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "EVENT_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        tempFilePath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1:
                    imageUri = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                        imageProfile.setImageBitmap(bitmap);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    Glide.with(ProfileActivity.this)
                            .load(tempFilePath)
                            .into(imageProfile);

                    imageUri = Uri.fromFile(new File(tempFilePath));
                    break;
            }

            sRef.child("user").child(mUser.getUid()).putFile(imageUri)
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            Dialog dialog = new Dialog(ProfileActivity.this);
                            dialog.setContentView(R.layout.dialog_upload);
                            dialog.show();
                        }
                    })
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            sRef.child("user").child(mUser.getUid()).getDownloadUrl()
                                    .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            dRef.child("user").child(mUser.getUid()).child("photoUrl").setValue(uri.toString());
                                            Dialog dialog = new Dialog(ProfileActivity.this);
                                            dialog.setContentView(R.layout.dialog_tersimpan);
                                            dialog.show();
                                            recreate();
                                        }
                                    });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Dialog dialog = new Dialog(ProfileActivity.this);
                            dialog.setContentView(R.layout.dialog_field_error);
                            TextView errorText = dialog.findViewById(R.id.textError);
                            errorText.setText("Gagal Menyimpan Data, Coba Beberapa Saat Lagi");
                            dialog.show();
                        }
                    });
        }
    }

    public void simpanPerubahan(View view) {
        if (TextUtils.isEmpty(namaProfile.getText()) || TextUtils.isEmpty(telponProfile.getText())) {
            Dialog dialog = new Dialog(ProfileActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView errorText = dialog.findViewById(R.id.textError);
            errorText.setText("Kolom tidak boleh kosong");
            dialog.show();
        } else {
            DatabaseReference userRef = dRef.child("user").child(mUser.getUid());
            userRef.child("nama").setValue(namaProfile.getText().toString());
            userRef.child("telpon").setValue(telponProfile.getText().toString());

            Dialog dialog = new Dialog(ProfileActivity.this);
            dialog.setContentView(R.layout.dialog_tersimpan);
            dialog.show();
        }
    }

    public void gantiPassword(View view) {
        if (TextUtils.isEmpty(passwordProfile.getText())) {
            Dialog dialog = new Dialog(ProfileActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView errorText = dialog.findViewById(R.id.textError);
            errorText.setText("Kolom tidak boleh kosong");
            dialog.show();
        } else if (passwordProfile.getText().toString().length() < 8) {
            Dialog dialog = new Dialog(ProfileActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView errorText = dialog.findViewById(R.id.textError);
            errorText.setText("Password Minimal 8 Karakter");
            dialog.show();
        } else {
            AuthCredential credential = EmailAuthProvider
                    .getCredential(sharedPref.getString(SharedPref.EMAIL),"1q2w3e4r5t");

            mUser.reauthenticate(credential)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            mUser.updatePassword(passwordProfile.getText().toString())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            sharedPref.setString(SharedPref.PASSWORD, passwordProfile.getText().toString());
                                            Dialog dialog = new Dialog(ProfileActivity.this);
                                            dialog.setContentView(R.layout.dialog_tersimpan);
                                            dialog.show();
                                        }
                                    });
                        }
                    });
        }
    }
}
