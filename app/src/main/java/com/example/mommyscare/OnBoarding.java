package com.example.android.mommycare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.android.mommycare.Support.SharedPref;
import com.example.android.mommycare.Support.SliderAdapterOnboarding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class OnBoarding extends AppCompatActivity {

    private ViewPager mSlideViewPager;
    private LinearLayout dotLayout;
    private SliderAdapterOnboarding sliderAdapter;

    private TextView[] mdots;

    private Button mNextBtn;
    private Button mPrevBtn;
    private Button finishBtn;

    private int mCurrentPage;

    FirebaseUser mUser;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);

        mUser = FirebaseAuth.getInstance().getCurrentUser();
        sharedPref = new SharedPref(this);

        mSlideViewPager = findViewById(R.id.slideViewPager);
        dotLayout = findViewById(R.id.dotsLayout);

        mNextBtn = (Button) findViewById(R.id.nextBtn);
        mPrevBtn = (Button) findViewById(R.id.prevBtn);
        finishBtn = (Button) findViewById(R.id.finishBtn);

        sliderAdapter = new SliderAdapterOnboarding(this);
        mSlideViewPager.setAdapter(sliderAdapter);


        addDotsIndicator(0);

        mSlideViewPager.addOnPageChangeListener(viewListener);

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSlideViewPager.setCurrentItem(mCurrentPage + 1);
            }
        });


        mPrevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSlideViewPager.setCurrentItem(mCurrentPage - 1);
            }
        });


    }

    public void addDotsIndicator(int position) {
        mdots = new TextView[2];
        dotLayout.removeAllViews();

        for (int i = 0; i < mdots.length; i++) {

            mdots[i] = new TextView(this);
            mdots[i].setText(Html.fromHtml("&#8226"));
            mdots[i].setTextSize(35);
            mdots[i].setTextColor(getResources().getColor(R.color.colorWhite));

            dotLayout.addView(mdots[i]);

        }

        if (mdots.length > 0) {
            mdots[position].setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float p, int posiels) {

        }

        @Override
        public void onPageSelected(int i) {
            addDotsIndicator(i);
            mCurrentPage = 1;

            if (i==0){
                mNextBtn.setEnabled(true);
                mPrevBtn.setEnabled(false);
                finishBtn.setEnabled(false);
                mNextBtn.setVisibility((View.VISIBLE));
                finishBtn.setVisibility((View.INVISIBLE));
                mPrevBtn.setVisibility((View.INVISIBLE));

                mNextBtn.setText("Next");
                mPrevBtn.setText("");
            }else if(i== mdots.length - 1){
                mNextBtn.setEnabled(false);
                mPrevBtn.setEnabled(true);
                finishBtn.setEnabled(true);
                mPrevBtn.setVisibility((View.VISIBLE));
                mNextBtn.setVisibility((View.INVISIBLE));
                finishBtn.setVisibility((View.VISIBLE));



                mNextBtn.setText("Finish");
                mPrevBtn.setText("back");

            }else{mNextBtn.setEnabled(true);
                mPrevBtn.setEnabled(true);
                mPrevBtn.setVisibility((View.VISIBLE));


                mNextBtn.setText("NEXT");
                mPrevBtn.setText("back");

            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public void finish(View view) {
        sharedPref.setBoolean(SharedPref.formatKey(SharedPref.NOT_FIRST_TIME, mUser.getUid()), true);
        Intent intent = new Intent(OnBoarding.this,MainActivity.class);
        startActivity(intent);

    }
}
