package com.example.android.mommycare;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.example.android.mommycare.Support.SupportClickListener;
import com.example.android.mommycare.ui.DetailHospitalFragment;
import com.example.android.mommycare.ui.DetailInformasiFragment;
import com.example.android.mommycare.ui.DetailSewaFragment;
import com.example.android.mommycare.ui.DokterFragment;
import com.example.android.mommycare.ui.HospitalFragment;
import com.example.android.mommycare.ui.HubungiDokterFragment;
import com.example.android.mommycare.ui.InformasiFragment;
import com.example.android.mommycare.ui.PembayaranFragment;
import com.example.android.mommycare.ui.PengaturanFragment;
import com.example.android.mommycare.ui.SewaFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    FrameLayout navHostFragment;
    LinearLayout informasiLayout, hospitalLayout, dokterLayout, sewaLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        // AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
         //       R.id.navigation_informasi, R.id.navigation_hospital, R.id.navigation_dokter, R.id.navigation_sewa)
         //       .build();
        // NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        // NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        // NavigationUI.setupWithNavController(navView, navController);

        navHostFragment = findViewById(R.id.frameLayout);

        informasiLayout = findViewById(R.id.informasiLayout);
        informasiLayout.setOnClickListener(this);
        hospitalLayout = findViewById(R.id.hospitalLayout);
        hospitalLayout.setOnClickListener(this);
        dokterLayout = findViewById(R.id.dokterLayout);
        dokterLayout.setOnClickListener(this);
        sewaLayout = findViewById(R.id.sewaLayout);
        sewaLayout.setOnClickListener(this);

//        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
//            @Override
//            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
//                switch (destination.getId()) {
//                    case R.id.navigation_informasi:
//                        textTop.setText("Informasi Tentang Janin");
//                        break;
//                    case R.id.navigation_hospital:
//                        textTop.setText("Informasi Rumah Sakit");
//                        break;
//                    case R.id.navigation_dokter:
//                        textTop.setText("Daftar Dokter");
//                        break;
//                    case R.id.navigation_sewa:
//                        textTop.setText("Sewa");
//                        break;
//                }
//            }
//        });
    }

    private void loadFragment(Fragment fragment, String tag, String args) {

        if (args != null) {
            Bundle data = new Bundle();
            data.putString("uid",args);
            fragment.setArguments(data);
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout,fragment)
                .addToBackStack(tag)
                .commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.informasiLayout:
                loadFragment(new InformasiFragment(topBarClickListener),"Informasi", null);
                break;
            case R.id.hospitalLayout:
                loadFragment(new HospitalFragment(topBarClickListener),"Hospital", null);
                break;
            case R.id.dokterLayout:
                loadFragment(new DokterFragment(topBarClickListener),"Dokter",null);
                break;
            case R.id.sewaLayout:
                loadFragment(new SewaFragment(topBarClickListener),"Sewa",null);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getIntent().hasExtra("pengaturan")) {
            loadFragment(new PengaturanFragment(topBarClickListener),"Pengaturan",null);
        } else {
            loadFragment(new InformasiFragment(topBarClickListener),"Informasi",null);
        }
    }

    SupportClickListener topBarClickListener = new SupportClickListener() {
        @Override
        public void onSetting() {
            loadFragment(new PengaturanFragment(topBarClickListener),"Pengaturan", null);
        }

        @Override
        public void onBack() {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                loadFragment(new InformasiFragment(topBarClickListener), "Informasi", null);
            }
        }

        @Override
        public void onRecyclerClick(int tipe, String uid) {
            switch (tipe) {
                case 0:
                    loadFragment(new DetailInformasiFragment(topBarClickListener), "Detail Informasi", uid);
                    break;
                case 1:
                    loadFragment(new DetailHospitalFragment(topBarClickListener), "Detail Hospital", uid);
                    break;
                case 2:
                    loadFragment(new HubungiDokterFragment(topBarClickListener), "Hubungi Dokter", uid);
                    break;
                case 3:
                    loadFragment(new DetailSewaFragment(topBarClickListener), "Sewa Barang", uid);
            }
        }

        @Override
        public void onSewaClick(String uid) {
            loadFragment(new PembayaranFragment(topBarClickListener), "Pembayaran", uid);
        }
    };
}
