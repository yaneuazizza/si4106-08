package com.example.android.mommycare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

public class SignupSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_success);
    }

    public void toLogin(View view) {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(SignupSuccessActivity.this, LoginActivity.class));
    }
}
