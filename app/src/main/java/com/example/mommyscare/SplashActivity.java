package com.example.android.mommycare;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.android.mommycare.Support.PopulateDB;
import com.example.android.mommycare.Support.SharedPref;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SplashActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        PopulateDB.populate();

        mAuth = FirebaseAuth.getInstance();
        sharedPref = new SharedPref(this);

        if (mAuth.getCurrentUser() != null && sharedPref.getBoolean(SharedPref.formatKey(SharedPref.REMEMBER,mAuth.getCurrentUser().getUid()))) {
            mAuth.signInWithEmailAndPassword(sharedPref.getString(SharedPref.EMAIL), "12345678")
                    .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            startActivity(new Intent(SplashActivity.this, MainActivity.class));
                            finish();
                        }
                    });
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent HomeIntent = new Intent(SplashActivity.this, WelcomeActivity.class );
                    startActivity(HomeIntent);
                    finish();

                }
            },2000);
        }
    }
}
