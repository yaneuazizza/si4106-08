package com.example.android.mommycare.Support;

import com.example.android.mommycare.Model.Dokter;
import com.example.android.mommycare.Model.Hospital;
import com.example.android.mommycare.Model.Informasi;
import com.example.android.mommycare.Model.Sewa;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PopulateDB {

    public static void populate() {
        DatabaseReference dRef = FirebaseDatabase.getInstance().getReference();
        List<Hospital> hospitalList = new ArrayList<>();
        hospitalList.add(new Hospital(
                "hospital1",
                "Muhammadiyah Hospital Bandung",
                "Private Hospital",
                "Jl. K.H. Ahmad Dahlan No. 53",
                "Open 24 hours",
                "https://firebasestorage.googleapis.com/v0/b/mommycare-3c8ec.appspot.com/o/hospital%2Fmuhammadiyah.jpg?alt=media&token=dc3f8dcc-b6c8-4b65-b9fc-504249f2f1df",
                "(022) 7301062"
        ));

        hospitalList.add(new Hospital(
                "hospital2",
                "Rumah Sakit Bina Sehat",
                "General Hospital",
                "Jl. Raya Dayeuhkolot No. 325",
                "Open 24 hours",
                "https://firebasestorage.googleapis.com/v0/b/mommycare-3c8ec.appspot.com/o/hospital%2Fbinasehat.jpg?alt=media&token=da9cc914-3b20-466d-8f6e-1ec5e2c521d6",
                "(022) 5207965"
        ));

        hospitalList.add(new Hospital(
                "hospital3",
                "Aliyah Medika",
                "Hospital",
                "Jl. Sukabirus No. 32",
                "Open 24 hours",
                "https://firebasestorage.googleapis.com/v0/b/mommycare-3c8ec.appspot.com/o/hospital%2Faliyahmedika.png?alt=media&token=8af3dd7c-7c7e-4d95-ac27-3ff9199e713b",
                "(022) 7569149"
        ));

        hospitalList.add(new Hospital(
                "hospital4",
                "Sartika Asih Hospital",
                "General Hospital",
                "Jl. Moch. Toha No. 369",
                "Open 24 hours",
                "https://firebasestorage.googleapis.com/v0/b/mommycare-3c8ec.appspot.com/o/hospital%2Fsartikaasih.jpg?alt=media&token=bce88ed4-932b-4871-a13b-06bc72cfb215",
                "(022) 5229544"
        ));

        for (Hospital hospital : hospitalList) {
            dRef.child("hospital").child(hospital.getUid()).setValue(hospital);
        }

        List<Dokter> dokterList = new ArrayList<>();
        dokterList.add(new Dokter(
                "dokter1",
                "dr. Ridwan, Sp.OG",
                "Dokter Spesialis Kebidanan dan Kandungan",
                "Muhammadiyah Hospital Bandung",
                "08121314151617",
                "https://firebasestorage.googleapis.com/v0/b/mommycare-3c8ec.appspot.com/o/dokter%2Fridwan.jpg?alt=media&token=078854a6-0b34-43c5-a620-724a46333d21"
        ));

        dokterList.add(new Dokter(
                "dokter2",
                "Dr. dr. Taufik Jamaan, Sp.OG",
                "Dokter Spesialis Kebidanan dan Kandungan - Fertilitas Endokrinologi Reproduksi",
                "Rumah Sakit Bina Sehat",
                "087887781234",
                "https://firebasestorage.googleapis.com/v0/b/mommycare-3c8ec.appspot.com/o/dokter%2Ftaufik.jpg?alt=media&token=0252affd-6b4b-46c4-9967-e5daaf632677"
        ));

        dokterList.add(new Dokter(
                "dokter3",
                "dr. Dewi Prabarini, Sp.OG",
                "Dokter Spesialis KEbidanan dan Kandungan",
                "Aliyah Medika",
                "085810293847",
                "https://firebasestorage.googleapis.com/v0/b/mommycare-3c8ec.appspot.com/o/dokter%2Fdewi.jpg?alt=media&token=cd463781-8d55-4eaa-adaf-b0a0479a93b1"
        ));

        for (Dokter dokter : dokterList) {
            dRef.child("dokter").child(dokter.getUid()).setValue(dokter);
        }

        Sewa sewa = new Sewa(
                "sewa1",
                "Maternity Belt",
                "Korset Hamil merek Tally kode 589 ini terbuat" +
                " dari bahan berkualitas yang kuat dan ringan." +
                 "Sangat cocok dipakai untuk ibu hamil " +
                 "yang tetap aktif beraktifitas.",
                "125000",
                "6",
                "https://firebasestorage.googleapis.com/v0/b/mommycare-3c8ec.appspot.com/o/sewa%2Fmaternity_belt.jpg?alt=media&token=d152c80e-4b22-49bf-a643-ae4c90023b44"
        );

        dRef.child("sewa").child(sewa.getUid()).setValue(sewa);

        List<Informasi> informasiList = new ArrayList<>();
        informasiList.add(new Informasi(
                "informasi1",
                "1",
                "Pada perinsipnya, makanan yang baik untuk ibu hamil 1 bulan harus memenuhi 4 sehat 5 sempurna setiap harinya:\n" +
                        "\n" +
                        "\t\t *karbohidrat, misalnya : nasi, roti, kentang, oatmeal\n" +
                        "\t\t *sayuran\n" +
                        "\t\t *buah-buahan\n" +
                        "\t\t *protein, misalnya : ayam, sapi, telur, tahu, tempe\n" +
                        "\t\t *produk susu, misalnya : susu, yogurt, keju\n" +
                        "\n" +
                        "Makanan lain yang harus dihindari adalah :\n" +
                        "\n" +
                        "\t\t *alkohol\n" +
                        "\t\t *mayonaise, karena mengandung telur mentah\n" +
                        "\t\t *telur yang kurang matang\n" +
                        "\t\t *daging yang kurang matang\n" +
                        "\t\t *sushi\n" +
                        "\t\t *produk susu yang tidak dipasteurisasi terlebih dahulu\n" +
                        "\t\t *seafood yang kandungan merkurinya tinggi, misalnya  kerang\n" +
                        "\n" +
                        "Beberapa tips untuk memehuni kebutuhan makanan yang baik untuk ibu hamil 1 bulan:\n" +
                        "\n" +
                        "\t\t *Sebaiknya, porsi sayuran dan buah-buahan lebih banyak daripada porsi karbohidrat.\n" +
                        "\t\t *Makanlah dalam porsi kecil-kecil namun sering, untuk mengurangi rasa mual.\n" +
                        "\t\t *Waktu makan yang dianjurkan adalah 5 kali, yaitu sarapan, makan siang, makan malam, serta diselip   cemilan di antara 3 kali makan utama tersebut.\n",
                "Olahraga yang aman bagi ibu hamil bulan pertama :\n" +
                        "\n" +
                        "\t\t * Yoga\n" +
                        "\t\t * Berlari\n" +
                        "\t\t * Berenang \n" +
                        "\t\t * Bersepeda\n" +
                        "\t\t * Senam Aerobik\n" +
                        "\t\t * Stretching\n" +
                        "\t\t * Senam Kegel",
                "Jika terlambat haid atau periode menstruasi tidak \n" +
                        "teratur, maka lakukanlah tes kehamilan dengan \n" +
                        "tespek di rumah. Jika hasilnya positif, maka sampai \n" +
                        "di sini Anda dinyatakan 'mungkin hamil'. Untuk \n" +
                        "memastikan status kehamilan Anda, maka \n" +
                        "periksalah ke dokter, yakni pada usia kehamilan 8 \n" +
                        "sampai 12 minggu yang dihitung dari HPHT.\n" +
                        "\n" +
                        "Jika tes negatif dan haid terlambat, maka Anda \n" +
                        "perlu mengulang lagi pemeriksaan seminggu \n" +
                        "kemudian. Beberapa wanita perlu waktu 2 sampai \n" +
                        "3 minggu setelah telat haid untuk memproduksi \n" +
                        "hormon kehamilan yang bisa terdeteksi pada alat \n" +
                        "tes kehamilan.\n" +
                        "\n" +
                        "\t\t * Hanya tespek yang bisa dilakukan untuk \n" +
                        "memeriksa kemungkinan hamil di usia 1 bulan ini."
        ));

        for (Informasi informasi : informasiList) {
            dRef.child("informasi").child(informasi.getUid()).setValue(informasi);
        }
    }

}
