package com.example.android.mommycare.Support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.android.mommycare.R;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class SliderAdapterOnboarding extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;


    public SliderAdapterOnboarding(Context context) {
        this.context = context;

    }

    public int[] slide_images = {
            R.drawable.first_slide, R.drawable.second_slide
    };

    public String[] slide_headings = {
            "Hi Bunda !",
            "Segala Informasi Tentang Kehamilan Bunda"
    };

    public String[] slide_desc = {
            "Mommy care akan memberikan informasi tentang kehamilan yang tervalidasi dari medis sesuai dengan masa kehamilan Bunda.",
            "Mommy care akan membantu \n" +
                    "bunda untuk mendapatkan \n" +
                    "informasi seputar kehamilan dan \n" +
                    "informasi-informasi pendukungnya \n" +
                    "untuk segala kenyamanan bunda."
    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView slideImageView = (ImageView) view.findViewById(R.id.imageView);
        TextView slideHeading = (TextView) view.findViewById(R.id.headingslide);
        TextView slideDesc = (TextView) view.findViewById(R.id.descriptiononboard);

        slideImageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        slideDesc.setText(slide_desc[position]);

        container.addView(view);

        return view;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}