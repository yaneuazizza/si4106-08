package com.example.android.mommycare.Support;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.mommycare.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class InformasiAdapter extends RecyclerView.Adapter<InformasiAdapter.InformasiViewHolder> {

    Context context;
    List<Drawable> informasiList = new ArrayList<>();
    SupportClickListener topBarClickListener;

    public InformasiAdapter(@NonNull Context context , SupportClickListener topBarClickListener) {
        this.context = context;
        this. topBarClickListener = topBarClickListener;
        informasiList.add(context.getDrawable(R.drawable.bayi1));
        informasiList.add(context.getDrawable(R.drawable.bayi2));
        informasiList.add(context.getDrawable(R.drawable.bayi3));
        informasiList.add(context.getDrawable(R.drawable.bayi4));
        informasiList.add(context.getDrawable(R.drawable.bayi5));
        informasiList.add(context.getDrawable(R.drawable.bayi6));
        informasiList.add(context.getDrawable(R.drawable.bayi7));
        informasiList.add(context.getDrawable(R.drawable.bayi8));
        informasiList.add(context.getDrawable(R.drawable.bayi9));
    }

    @NonNull
    @Override
    public InformasiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.informasi_card, parent, false);
        return new InformasiViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull InformasiViewHolder holder, int position) {
        holder.informasiImage.setImageDrawable(informasiList.get(position));
        holder.informasiText.setText(context.getResources().getString(R.string.informasi_card,String.valueOf(position+1)));
    }

    @Override
    public int getItemCount() {
        return informasiList.size();
    }

    public class InformasiViewHolder extends RecyclerView.ViewHolder {
        ImageView informasiImage;
        TextView informasiText;
        public InformasiViewHolder(@NonNull View itemView) {
            super(itemView);
            informasiImage = itemView.findViewById(R.id.informasiImage);
            informasiText = itemView.findViewById(R.id.informasiText);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uid = "informasi"+(getAdapterPosition()+1);
                    topBarClickListener.onRecyclerClick(topBarClickListener.INFORMASI, uid);
                    Log.d("informasiUID","informasi"+getAdapterPosition());
                }
            });
        }
    }
}
