package com.example.android.mommycare.Support;

public interface SupportClickListener {

    int INFORMASI = 0;
    int HOSPITAL = 1;
    int DOKTER = 2;
    int SEWA = 3;

    void onSetting();

    void onBack();

    void onRecyclerClick(int tipe, String uid);

    void onSewaClick(String uid);
}
