package com.example.android.mommycare.Support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.mommycare.Model.Sewa;
import com.example.android.mommycare.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SewaAdapter extends RecyclerView.Adapter<SewaAdapter.SewaViewHolder> {

    Context context;
    List<Sewa> sewaList;
    SupportClickListener topBarClickListener;

    public SewaAdapter(Context context, List<Sewa> sewaList, SupportClickListener topBarClickListener) {
        this.context = context;
        this.sewaList = sewaList;
        this.topBarClickListener = topBarClickListener;
    }

    @NonNull
    @Override
    public SewaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.sewa_card, parent, false);
        return new SewaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SewaViewHolder holder, int position) {
        Sewa sewa = sewaList.get(position);
        Glide.with(context)
                .load(sewa.getPhotoURl())
                .into(holder.sewaFoto);
        holder.sewaNama.setText(sewa.getNama());
        holder.sewaKeterangan.setText(sewa.getKeterangan());
    }

    @Override
    public int getItemCount() {
        return sewaList.size();
    }

    public class SewaViewHolder extends RecyclerView.ViewHolder {
        ImageView sewaFoto;
        TextView sewaNama, sewaKeterangan, klikUntukDetail;
        public SewaViewHolder(@NonNull View itemView) {
            super(itemView);
            sewaFoto = itemView.findViewById(R.id.sewaFoto);
            sewaNama = itemView.findViewById(R.id.sewaNama);
            sewaKeterangan = itemView.findViewById(R.id.sewaKeterangan);
            klikUntukDetail = itemView.findViewById(R.id.klikUntukDetail);

            klikUntukDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uid = sewaList.get(getAdapterPosition()).getUid();
                    topBarClickListener.onRecyclerClick(topBarClickListener.SEWA, uid);
                }
            });
        }
    }
}
