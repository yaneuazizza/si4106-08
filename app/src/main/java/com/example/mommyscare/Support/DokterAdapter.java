package com.example.android.mommycare.Support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.mommycare.Model.Dokter;
import com.example.android.mommycare.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class DokterAdapter extends RecyclerView.Adapter<DokterAdapter.DokterViewHolder> {

    Context context;
    List<Dokter> dokterList;
    SupportClickListener topBarClickListener;

    public DokterAdapter(Context context, List<Dokter> dokterList, SupportClickListener topBarClickListener) {
        this.context = context;
        this.dokterList = dokterList;
        this.topBarClickListener = topBarClickListener;
    }

    @NonNull
    @Override
    public DokterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.dokter_card, parent,false);
        return new DokterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DokterViewHolder holder, int position) {
        Dokter dokter = dokterList.get(position);
        Glide.with(context)
                .load(dokter.getPhotoUrl())
                .into(holder.fotoDokter);
        holder.namaDokter.setText(dokter.getNama());
        holder.keteranganDokter.setText(dokter.getKeterangan());
        holder.hospitalDokter.setText(dokter.getHospital());
    }

    @Override
    public int getItemCount() {
        return dokterList.size();
    }

    public class DokterViewHolder extends RecyclerView.ViewHolder {
        ImageView fotoDokter;
        TextView namaDokter, keteranganDokter, hospitalDokter;
        Button hubungiDokter;
        public DokterViewHolder(@NonNull View itemView) {
            super(itemView);
            fotoDokter = itemView.findViewById(R.id.fotoDokter);
            namaDokter = itemView.findViewById(R.id.namaDokter);
            keteranganDokter = itemView.findViewById(R.id.keteranganDokter);
            hospitalDokter = itemView.findViewById(R.id.hospitalDokter);
            hubungiDokter = itemView.findViewById(R.id.hubungiDokter);

            hubungiDokter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uid = dokterList.get(getAdapterPosition()).getUid();
                    topBarClickListener.onRecyclerClick(topBarClickListener.DOKTER, uid);
                }
            });
        }
    }
}
