package com.example.android.mommycare.Support;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.mommycare.Model.Hospital;
import com.example.android.mommycare.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HospitalAdapter extends RecyclerView.Adapter<HospitalAdapter.HospitalViewHolder> {

    Context context;
    List<Hospital> hospitalList;
    SupportClickListener topBarClickListener;

    public HospitalAdapter(Context context, List<Hospital> hospitalList, SupportClickListener topBarClickListener) {
        this.context = context;
        this.hospitalList = hospitalList;
        this.topBarClickListener = topBarClickListener;
    }

    @NonNull
    @Override
    public HospitalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.hospital_card,parent,false);
        return new HospitalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HospitalViewHolder holder, int position) {
        Hospital hospital = hospitalList.get(position);
        holder.namaHospital.setText(hospital.getNama());
        holder.namaHospitalCard.setText(hospital.getNama());
        holder.jenisHospital.setText(hospital.getJenis());
        holder.jamKerjaHospital.setText(hospital.getJamKerja());
        holder.alamatTelponHospital.setText(context.getResources().getString(
                R.string.hospital_alamat_phone,
                hospital.getAlamat(),
                hospital.getTelpon()
        ));
    }

    @Override
    public int getItemCount() {
        return hospitalList.size();
    }

    public class HospitalViewHolder extends RecyclerView.ViewHolder {
        TextView namaHospital, namaHospitalCard, jenisHospital, alamatTelponHospital, jamKerjaHospital, lihatLebih;
        public HospitalViewHolder(@NonNull View itemView) {
            super(itemView);
            namaHospital = itemView.findViewById(R.id.namaHospital);
            namaHospitalCard = itemView.findViewById(R.id.namaHospitalCard);
            jenisHospital = itemView.findViewById(R.id.jenisHospital);
            jamKerjaHospital = itemView.findViewById(R.id.jamKerjaHospital);
            alamatTelponHospital = itemView.findViewById(R.id.alamatTelponHospital);
            lihatLebih = itemView.findViewById(R.id.lihatLebih);

            lihatLebih.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uid = hospitalList.get(getAdapterPosition()).getUid();
                    topBarClickListener.onRecyclerClick(topBarClickListener.HOSPITAL, uid);
                }
            });
        }
    }
}
