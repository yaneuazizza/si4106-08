package com.example.android.mommycare;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.android.mommycare.Model.User;
import com.example.android.mommycare.Support.SharedPref;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignupActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    DatabaseReference dRef;
    SharedPref sharedPref;
    EditText namaDepan, namaBelakang, email, password, konfirmasiPassword;
    CheckBox checkBoxPrivacyPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mAuth = FirebaseAuth.getInstance();
        dRef = FirebaseDatabase.getInstance().getReference();
        sharedPref = new SharedPref(this);

        namaDepan = findViewById(R.id.namaDepan);
        namaBelakang = findViewById(R.id.namaBelakang);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        konfirmasiPassword = findViewById(R.id.konfirmasiPassword);
        checkBoxPrivacyPolicy = findViewById(R.id.checkBoxPrivacyPolicy);
    }

    public void toLogin(View view) {
        startActivity(new Intent(SignupActivity.this, LoginActivity.class));
    }

    public void signup(View view) {
        if (TextUtils.isEmpty(namaDepan.getText()) ||
                TextUtils.isEmpty(namaBelakang.getText()) ||
                TextUtils.isEmpty(email.getText()) ||
                TextUtils.isEmpty(password.getText()) ||
                TextUtils.isEmpty(konfirmasiPassword.getText())
            )
        {
            Dialog dialog = new Dialog(SignupActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView errorText = dialog.findViewById(R.id.textError);
            errorText.setText("Kolom tidak boleh kosong");
            dialog.show();
        } else if (!password.getText().toString().equals(konfirmasiPassword.getText().toString())) {
            Dialog dialog = new Dialog(SignupActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView errorText = dialog.findViewById(R.id.textError);
            errorText.setText("Password tidak sama");
            dialog.show();
        } else if (password.getText().toString().length() < 8) {
            Dialog dialog = new Dialog(SignupActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView errorText = dialog.findViewById(R.id.textError);
            errorText.setText("Password minimal 8 karakter");
            dialog.show();
        } else if (!checkBoxPrivacyPolicy.isChecked()) {
            Dialog dialog = new Dialog(SignupActivity.this);
            dialog.setContentView(R.layout.dialog_field_error);
            TextView errorText = dialog.findViewById(R.id.textError);
            errorText.setText("Harap setujui Privacy Policy");
            dialog.show();
        } else {
            mAuth.createUserWithEmailAndPassword(email.getText().toString(), konfirmasiPassword.getText().toString())
                    .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            sharedPref.setString(SharedPref.EMAIL, email.getText().toString());
                            sharedPref.setString(SharedPref.PASSWORD, konfirmasiPassword.getText().toString());

                            User user = new User(
                                    mAuth.getCurrentUser().getUid(),
                                    email.getText().toString(),
                                    namaDepan.getText().toString() + " " +namaBelakang.getText().toString(),
                                    "081213141516",
                                    "https://firebasestorage.googleapis.com/v0/b/mommycare-3c8ec.appspot.com/o/user%2Fuserdefault.png?alt=media&token=23c9b22f-629f-44d5-b958-b618a76a3511"
                            );

                            dRef.child("user").child(user.getUid()).setValue(user)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            startActivity(new Intent(SignupActivity.this, SignupSuccessActivity.class));
                                            finish();
                                        }
                                    });
                        }
                    })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Dialog dialog = new Dialog(SignupActivity.this);
                    dialog.setContentView(R.layout.dialog_field_error);
                    TextView errorText = dialog.findViewById(R.id.textError);
                    errorText.setText(e.getLocalizedMessage());
                    dialog.show();
                }
            });
        }
    }
}
