package com.example.android.mommycare.ui;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.mommycare.Model.Hospital;
import com.example.android.mommycare.R;
import com.example.android.mommycare.Support.SupportClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailHospitalFragment extends Fragment {

    DatabaseReference dRef;
    SupportClickListener topBarClickListener;
    ImageView backArrow, fotoHospital;
    TextView namaHospital, alamatHospital, jamKerjaHospital, telponHospital;
    Button mapsIntent;
    Uri mapsUri;

    public DetailHospitalFragment() {
        // Required empty public constructor
    }

    public DetailHospitalFragment(SupportClickListener topBarClickListener) {
        this.topBarClickListener = topBarClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_detail_hospital, container, false);

        backArrow = root.findViewById(R.id.backArrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topBarClickListener.onBack();
            }
        });

        namaHospital = root.findViewById(R.id.namaHospital);
        fotoHospital = root.findViewById(R.id.fotoHospital);
        alamatHospital = root.findViewById(R.id.alamatHospital);
        telponHospital = root.findViewById(R.id.telponHospital);

        String args = getArguments().getString("uid");
        dRef = FirebaseDatabase.getInstance().getReference();
        dRef.child("hospital").child(args).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Hospital hospital = dataSnapshot.getValue(Hospital.class);
                namaHospital.setText(hospital.getNama());
                alamatHospital.setText(hospital.getAlamat());
                telponHospital.setText(hospital.getTelpon());

                Glide.with(getContext())
                        .load(hospital.getPhotoUrl())
                        .into(fotoHospital);

                mapsUri = hospital.location();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mapsIntent = root.findViewById(R.id.mapsIntent);
        mapsIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Intent.ACTION_VIEW, mapsUri);
                if (in.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(in);
                } else {
                    Dialog dialog = new Dialog(getContext());
                    dialog.setContentView(R.layout.dialog_field_error);
                    TextView errorText = dialog.findViewById(R.id.textError);
                    errorText.setText("Tidak Bisa Membuka Google Maps");
                    dialog.show();
                }
            }
        });


        return root;
    }
}
