package com.example.android.mommycare.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.android.mommycare.Pengaturan.FeedbackActivity;
import com.example.android.mommycare.Pengaturan.PengaturanActivity;
import com.example.android.mommycare.Pengaturan.ProfileActivity;
import com.example.android.mommycare.R;
import com.example.android.mommycare.Support.SupportClickListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class PengaturanFragment extends Fragment implements View.OnClickListener {

    ImageView backArrow;
    SupportClickListener topBarClickListener;
    LinearLayout profileLayout, pengaturanLayout, feedbackLayout, exitLayout;

    public PengaturanFragment() {
    }

    public PengaturanFragment(SupportClickListener topBarClickListener) {
        this.topBarClickListener = topBarClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_pengaturan, container, false);
        backArrow = root.findViewById(R.id.backArrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topBarClickListener.onBack();
            }
        });

        profileLayout = root.findViewById(R.id.profileLayout);
        profileLayout.setOnClickListener(this);
        pengaturanLayout = root.findViewById(R.id.pengaturanLayout);
        pengaturanLayout.setOnClickListener(this);
        feedbackLayout = root.findViewById(R.id.feedbackLayout);
        feedbackLayout.setOnClickListener(this);
        exitLayout = root.findViewById(R.id.exitLayout);
        exitLayout.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profileLayout:
                startActivity(new Intent(getContext(), ProfileActivity.class));
                break;
            case R.id.pengaturanLayout:
                startActivity(new Intent(getContext(), PengaturanActivity.class));
                break;
            case R.id.feedbackLayout:
                startActivity(new Intent(getContext(), FeedbackActivity.class));
                break;
            case R.id.exitLayout:
                getActivity().finishAndRemoveTask();
                break;
        }
    }
}
