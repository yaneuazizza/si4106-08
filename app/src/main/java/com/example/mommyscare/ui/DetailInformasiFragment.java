package com.example.android.mommycare.ui;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.mommycare.Model.Informasi;
import com.example.android.mommycare.R;
import com.example.android.mommycare.SignupActivity;
import com.example.android.mommycare.Support.SupportClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailInformasiFragment extends Fragment {

    SupportClickListener supportClickListener;
    ImageView backArrow;
    TextView textTop, makanan, olahraga, pemeriksaan;
    DatabaseReference dRef;
    String uid;

    public DetailInformasiFragment() {
        // Required empty public constructor
    }

    public DetailInformasiFragment(SupportClickListener supportClickListener) {
        this.supportClickListener = supportClickListener;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_detail_informasi, container, false);

        backArrow = root.findViewById(R.id.backArrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportClickListener.onBack();
            }
        });

        textTop = root.findViewById(R.id.textTop);
        makanan = root.findViewById(R.id.makanan);
        olahraga = root.findViewById(R.id.olahraga);
        pemeriksaan = root.findViewById(R.id.pemeriksaan);

        uid = getArguments().getString("uid");
        dRef = FirebaseDatabase.getInstance().getReference();
        dRef.child("informasi").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Informasi informasi = dataSnapshot.getValue(Informasi.class);
                    textTop.setText(getContext().getResources().getString(
                            R.string.infromasi_title,
                            informasi.getBulan_ke()
                    ));
                    makanan.setText(informasi.getMakanan());
                    olahraga.setText(informasi.getOlahraga());
                    pemeriksaan.setText(informasi.getPemeriksaan());
                } else {
                    Dialog dialog = new Dialog(getContext());
                    dialog.setContentView(R.layout.dialog_field_error);
                    TextView errorText = dialog.findViewById(R.id.textError);
                    errorText.setText("Informasi Belum Tersedia");
                    dialog.show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return root;
    }
}
