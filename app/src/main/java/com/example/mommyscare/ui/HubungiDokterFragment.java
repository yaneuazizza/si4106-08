package com.example.android.mommycare.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.mommycare.Model.Dokter;
import com.example.android.mommycare.R;
import com.example.android.mommycare.Support.SupportClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class HubungiDokterFragment extends Fragment {

    SupportClickListener topBarClickListener;
    DatabaseReference dRef;
    TextView namaDokter, keteranganDokter, hospitalDokter, telponDokter;
    ImageView fotoDokter, imageSetting, backArrow;

    public HubungiDokterFragment() {
        // Required empty public constructor
    }

    public HubungiDokterFragment(SupportClickListener topBarClickListener) {
        this.topBarClickListener = topBarClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_hubungi_dokter, container, false);

        namaDokter = root.findViewById(R.id.namaDokter);
        keteranganDokter = root.findViewById(R.id.keteranganDokter);
        hospitalDokter = root.findViewById(R.id.hospitalDokter);
        telponDokter = root.findViewById(R.id.telponDokter);
        fotoDokter = root.findViewById(R.id.fotoDokter);

        imageSetting = root.findViewById(R.id.imageSetting);
        imageSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topBarClickListener.onSetting();
            }
        });

        backArrow = root.findViewById(R.id.backArrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topBarClickListener.onBack();
            }
        });

        String args = getArguments().getString("uid");
        dRef = FirebaseDatabase.getInstance().getReference();
        dRef.child("dokter").child(args).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Dokter dokter = dataSnapshot.getValue(Dokter.class);
                namaDokter.setText(dokter.getNama());
                keteranganDokter.setText(dokter.getKeterangan());
                hospitalDokter.setText(dokter.getHospital());
                telponDokter.setText(dokter.getTelpon());
                Glide.with(getContext())
                        .load(dokter.getPhotoUrl())
                        .into(fotoDokter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return root;
    }
}
