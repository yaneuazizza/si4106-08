package com.example.android.mommycare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.mommycare.R;
import com.example.android.mommycare.Support.InformasiAdapter;
import com.example.android.mommycare.Support.SupportClickListener;

public class InformasiFragment extends Fragment {

    ImageView imageSetting;
    RecyclerView informasiList;
    SupportClickListener topBarClickListener;

    public InformasiFragment() {
    }

    public InformasiFragment(SupportClickListener topBarClickListener) {
        this.topBarClickListener = topBarClickListener;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_informasi, container, false);


        imageSetting = root.findViewById(R.id.imageSetting);
        imageSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topBarClickListener.onSetting();
            }
        });

        informasiList = root.findViewById(R.id.informasiRV);
        informasiList.setLayoutManager(new LinearLayoutManager(getContext()));
        InformasiAdapter informasiAdapter = new InformasiAdapter(getContext(),topBarClickListener);
        informasiList.setAdapter(informasiAdapter);
        return root;
    }

}
