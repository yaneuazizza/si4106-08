package com.example.android.mommycare.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.mommycare.Model.Sewa;
import com.example.android.mommycare.R;
import com.example.android.mommycare.Support.SupportClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailSewaFragment extends Fragment {

    SupportClickListener supportClickListener;
    DatabaseReference dRef;
    ImageView imageSetting, backArrow, sewaFoto;
    TextView sewaNama, sewaKeterangan, sewaBarang;
    String uid;

    public DetailSewaFragment() {
        // Required empty public constructor
    }

    public DetailSewaFragment(SupportClickListener supportClickListener) {
        this.supportClickListener = supportClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_detail_sewa, container, false);

        imageSetting = root.findViewById(R.id.imageSetting);
        imageSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportClickListener.onSetting();
            }
        });

        backArrow = root.findViewById(R.id.backArrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportClickListener.onBack();
            }
        });

        sewaFoto = root.findViewById(R.id.sewaFoto);
        sewaNama = root.findViewById(R.id.sewaNama);
        sewaKeterangan = root.findViewById(R.id.sewaKeterangan);

        dRef = FirebaseDatabase.getInstance().getReference();
        uid = getArguments().getString("uid");
        dRef.child("sewa").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Sewa sewa = dataSnapshot.getValue(Sewa.class);
                sewaNama.setText(sewa.getNama());
                sewaKeterangan.setText(sewa.getKeterangan());
                Glide.with(getContext())
                        .load(sewa.getPhotoURl())
                        .into(sewaFoto);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        sewaBarang = root.findViewById(R.id.sewaBarang);
        sewaBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportClickListener.onSewaClick(uid);
            }
        });

        return root;
    }
}
