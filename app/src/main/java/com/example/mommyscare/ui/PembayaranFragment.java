package com.example.android.mommycare.ui;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.android.mommycare.LoginActivity;
import com.example.android.mommycare.Model.Pembayaran;
import com.example.android.mommycare.Model.Sewa;
import com.example.android.mommycare.R;
import com.example.android.mommycare.Support.SupportClickListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class PembayaranFragment extends Fragment {

    SupportClickListener supportClickListener;
    FirebaseUser mUser;
    DatabaseReference dRef;
    String uid;
    ImageView imageSetting, backArrow, sewaFoto, bankTransfer;
    TextView sewaNama, sewaKeterangan;
    ConstraintLayout metodeLayout, pembayaranLayout;
    EditText sewaQty;

    public PembayaranFragment() {
        // Required empty public constructor
        
    }

    public PembayaranFragment(SupportClickListener supportClickListener) {
        this.supportClickListener = supportClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_pembayaran, container, false);

        imageSetting = root.findViewById(R.id.imageSetting);
        imageSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportClickListener.onSetting();
            }
        });

        backArrow = root.findViewById(R.id.backArrow);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportClickListener.onBack();
            }
        });

        sewaFoto = root.findViewById(R.id.sewaFoto);
        sewaNama = root.findViewById(R.id.sewaNama);
        sewaKeterangan = root.findViewById(R.id.sewaKeterangan);
        sewaQty = root.findViewById(R.id.sewaQty);

        mUser = FirebaseAuth.getInstance().getCurrentUser();
        dRef = FirebaseDatabase.getInstance().getReference();
        uid = getArguments().getString("uid");
        dRef.child("sewa").child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Sewa sewa = dataSnapshot.getValue(Sewa.class);
                sewaNama.setText(sewa.getNama());
                sewaKeterangan.setText(sewa.getKeterangan());
                Glide.with(getContext())
                        .load(sewa.getPhotoURl())
                        .into(sewaFoto);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        metodeLayout = root.findViewById(R.id.metodeLayout);
        pembayaranLayout = root.findViewById(R.id.pembayaranLayout);
        bankTransfer = root.findViewById(R.id.bankTransfer);
        bankTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                metodeLayout.setVisibility(View.GONE);
                pembayaranLayout.setVisibility(View.VISIBLE);

                String key = dRef.child("pembayaran").push().getKey();
                dRef.child("pembayaran").child(key)
                        .setValue(new Pembayaran(
                                key,
                                mUser.getUid(),
                                sewaNama.getText().toString(),
                                uid,
                                "Bank Transfer",
                                Integer.parseInt(sewaQty.getText().toString())
                        ));
            }
        });

        return root;
    }
}
