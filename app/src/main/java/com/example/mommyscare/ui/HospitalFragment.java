package com.example.android.mommycare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.mommycare.Model.Hospital;
import com.example.android.mommycare.R;
import com.example.android.mommycare.Support.HospitalAdapter;
import com.example.android.mommycare.Support.SupportClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HospitalFragment extends Fragment {

    ImageView imageSetting;
    SupportClickListener topBarClickListener;
    DatabaseReference dRef;
    RecyclerView hospitalRV;
    HospitalAdapter adapter;
    List<Hospital> hospitalList = new ArrayList<>();

    public HospitalFragment() {
    }

    public HospitalFragment(SupportClickListener topBarClickListener) {
        this.topBarClickListener = topBarClickListener;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_hospital, container, false);

        imageSetting = root.findViewById(R.id.imageSetting);
        imageSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topBarClickListener.onSetting();
            }
        });

        hospitalRV = root.findViewById(R.id.hospitalRV);
        hospitalRV.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new HospitalAdapter(getContext(), hospitalList, topBarClickListener);
        hospitalRV.setAdapter(adapter);

        dRef = FirebaseDatabase.getInstance().getReference();
        dRef.child("hospital").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    hospitalList.add(ds.getValue(Hospital.class));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return root;
    }
}
