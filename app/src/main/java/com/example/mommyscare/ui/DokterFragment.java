package com.example.android.mommycare.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.mommycare.Model.Dokter;
import com.example.android.mommycare.R;
import com.example.android.mommycare.Support.DokterAdapter;
import com.example.android.mommycare.Support.SupportClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DokterFragment extends Fragment {

    ImageView imageSetting;
    SupportClickListener topBarClickListener;
    DatabaseReference dRef;
    RecyclerView dokterRV;
    DokterAdapter adapter;
    List<Dokter> dokterList = new ArrayList<>();

    public DokterFragment() {
    }

    public DokterFragment(SupportClickListener topBarClickListener) {
        this.topBarClickListener = topBarClickListener;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dokter, container, false);

        imageSetting = root.findViewById(R.id.imageSetting);
        imageSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topBarClickListener.onSetting();
            }
        });

        dokterRV = root.findViewById(R.id.dokterRV);
        dokterRV.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new DokterAdapter(getContext(), dokterList, topBarClickListener);
        dokterRV.setAdapter(adapter);

        dRef = FirebaseDatabase.getInstance().getReference();
        dRef.child("dokter").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    dokterList.add(ds.getValue(Dokter.class));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return root;
    }
}
