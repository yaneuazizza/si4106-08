package com.example.android.mommycare.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.android.mommycare.Model.Sewa;
import com.example.android.mommycare.R;
import com.example.android.mommycare.Support.SewaAdapter;
import com.example.android.mommycare.Support.SupportClickListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SewaFragment extends Fragment {

    ImageView imageSetting;
    SupportClickListener topBarClickListener;
    DatabaseReference dRef;
    RecyclerView sewaRV;
    SewaAdapter adapter;
    List<Sewa> sewaList = new ArrayList<>();

    public SewaFragment() {
    }

    public SewaFragment(SupportClickListener topBarClickListener) {
        this.topBarClickListener = topBarClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_sewa, container, false);

        imageSetting = root.findViewById(R.id.imageSetting);
        imageSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topBarClickListener.onSetting();
            }
        });

        sewaRV = root.findViewById(R.id.sewaRV);
        sewaRV.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new SewaAdapter(getContext(), sewaList, topBarClickListener);
        sewaRV.setAdapter(adapter);

        dRef = FirebaseDatabase.getInstance().getReference();
        dRef.child("sewa").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    sewaList.add(ds.getValue(Sewa.class));
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return root;
    }
}
