package com.example.android.mommycare.Model;

public class Sewa {

    String uid;
    String nama;
    String keterangan;
    String harga;
    String qty;
    String photoURl;

    public Sewa() {
    }

    public Sewa(String uid, String nama, String keterangan, String harga, String qty, String photoURl) {
        this.uid = uid;
        this.nama = nama;
        this.keterangan = keterangan;
        this.harga = harga;
        this.qty = qty;
        this.photoURl = photoURl;
    }

    public String getUid() {
        return uid;
    }

    public String getNama() {
        return nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public String getHarga() {
        return harga;
    }

    public String getQty() {
        return qty;
    }

    public String getPhotoURl() {
        return photoURl;
    }
}
