package com.example.android.mommycare.Model;

public class Feedback {

    String uid;
    String nama;
    String subjek;
    String pesan;
    String kontak;

    public Feedback() {
    }

    public Feedback(String uid, String nama, String subjek, String pesan, String kontak) {
        this.uid = uid;
        this.nama = nama;
        this.subjek = subjek;
        this.pesan = pesan;
        this.kontak = kontak;
    }

    public String getUid() {
        return uid;
    }

    public String getNama() {
        return nama;
    }

    public String getSubjek() {
        return subjek;
    }

    public String getPesan() {
        return pesan;
    }

    public String getKontak() {
        return kontak;
    }
}
