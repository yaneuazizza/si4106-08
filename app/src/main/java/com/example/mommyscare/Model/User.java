package com.example.android.mommycare.Model;

public class User {

    String uid;
    String email;
    String nama;
    String telpon;
    String photoUrl;

    public User() {
    }

    public User(String uid, String email, String nama, String telpon) {
        this.uid = uid;
        this.email = email;
        this.nama = nama;
        this.telpon = telpon;
    }

    public User(String uid, String email, String nama, String telpon, String photoUrl) {
        this.uid = uid;
        this.email = email;
        this.nama = nama;
        this.telpon = telpon;
        this.photoUrl = photoUrl;
    }

    public String getUid() {
        return uid;
    }

    public String getEmail() {
        return email;
    }

    public String getNama() {
        return nama;
    }

    public String getTelpon() {
        return telpon;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }
}
