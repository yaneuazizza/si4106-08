package com.example.android.mommycare.Model;

public class Informasi {

    String uid;
    String bulan_ke;
    String makanan;
    String olahraga;
    String pemeriksaan;

    public Informasi() {
    }

    public Informasi(String uid, String bulan_ke, String makanan, String olahraga, String pemeriksaan) {
        this.uid = uid;
        this.bulan_ke = bulan_ke;
        this.makanan = makanan;
        this.olahraga = olahraga;
        this.pemeriksaan = pemeriksaan;
    }

    public String getUid() {
        return uid;
    }

    public String getBulan_ke() {
        return bulan_ke;
    }

    public String getMakanan() {
        return makanan;
    }

    public String getOlahraga() {
        return olahraga;
    }

    public String getPemeriksaan() {
        return pemeriksaan;
    }
}
