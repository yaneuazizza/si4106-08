package com.example.android.mommycare.Model;

import android.net.Uri;

public class Hospital {
    String uid;
    String nama;
    String jenis;
    String alamat;
    String jamKerja;
    String photoUrl;
    String telpon;

    public Hospital() {
    }

    public Hospital(String uid, String nama, String jenis, String alamat, String jamKerja, String photoUrl, String telpon) {
        this.uid = uid;
        this.nama = nama;
        this.jenis = jenis;
        this.alamat = alamat;
        this.jamKerja = jamKerja;
        this.photoUrl = photoUrl;
        this.telpon = telpon;
    }

    public String getUid() {
        return uid;
    }

    public String getNama() {
        return nama;
    }

    public String getJenis() {
        return jenis;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getJamKerja() {
        return jamKerja;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getTelpon() {
        return telpon;
    }

    public Uri location(){
        return Uri.parse("geo:0,0?q=" + nama);
    }
}
