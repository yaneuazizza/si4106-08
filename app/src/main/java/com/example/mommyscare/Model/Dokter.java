package com.example.android.mommycare.Model;

public class Dokter {

    String uid;
    String nama;
    String keterangan;
    String hospital;
    String telpon;
    String photoUrl;

    public Dokter() {
    }

    public Dokter(String uid, String nama, String keterangan, String hospital, String telpon, String photoUrl) {
        this.uid = uid;
        this.nama = nama;
        this.keterangan = keterangan;
        this.hospital = hospital;
        this.telpon = telpon;
        this.photoUrl = photoUrl;
    }

    public String getUid() {
        return uid;
    }

    public String getNama() {
        return nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public String getHospital() {
        return hospital;
    }

    public String getTelpon() {
        return telpon;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }
}
