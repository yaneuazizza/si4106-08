package com.example.android.mommycare.Model;

public class Pembayaran {

    String uid;
    String pembayarUid;
    String namaBarang;
    String barangUid;
    String metode;
    int qty;

    public Pembayaran() {
    }

    public Pembayaran(String uid, String pembayarUid, String namaBarang, String barangUid, String metode, int qty) {
        this.uid = uid;
        this.pembayarUid = pembayarUid;
        this.namaBarang = namaBarang;
        this.barangUid = barangUid;
        this.metode = metode;
        this.qty = qty;
    }

    public String getUid() {
        return uid;
    }

    public String getPembayarUid() {
        return pembayarUid;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public String getBarangUid() {
        return barangUid;
    }

    public String getMetode() {
        return metode;
    }

    public int getQty() {
        return qty;
    }
}
